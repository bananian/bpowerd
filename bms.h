#ifndef _BPOWERD_BMS_H_
#define _BPOWERD_BMS_H_

int bms_open(void);
int bms_wait(int fd);
int get_charge(void);

#endif
