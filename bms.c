#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <linux/ioctl.h>
#include <errno.h>
#include "bms.h"

static int bdata = -1;

struct qpnp_vm_bms_data {
	unsigned int num_fifo;
	unsigned int fifo_uv[8];
	unsigned int sample_interval_ms;
	unsigned int sample_count;
	unsigned int acc_uv;
	unsigned int acc_count;
	unsigned int seq_num;
};

struct battery_params {
	int soc;
	int ocv_uv;
	int rbatt_sf;
	int batt_temp;
	int slope;
	int fcc_mah;
};

/*  IOCTLs to query battery profile data */
#define BPIOCXSOC	_IOWR('B', 0x01, struct battery_params) /* SOC */
#define BPIOCXRBATT	_IOWR('B', 0x02, struct battery_params) /* RBATT SF */
#define BPIOCXSLOPE	_IOWR('B', 0x03, struct battery_params) /* SLOPE */
#define BPIOCXFCC	_IOWR('B', 0x04, struct battery_params) /* FCC */

#define SYS_ROOT "/sys/class/power_supply/bms/"

int bms_open(void)
{
	if(bdata < 0) bdata = open("/dev/battery_data", O_RDWR|O_CLOEXEC);
	if(bdata < 0) syslog(LOG_ERR, "Failed to open battery_data: %s",
			strerror(errno));
	return open("/dev/vm_bms", O_RDONLY|O_CLOEXEC);
}

static int get_sys(const char *path)
{
	int val;
	FILE *f = fopen(path, "r");
	if(!f){
		syslog(LOG_ERR, "Failed to open '%s': %s",
				path, strerror(errno));
		return -errno;
	}
	if(fscanf(f, "%d", &val) == 1){
		fclose(f);
		if(val < 0){
			syslog(LOG_ERR, "'%s' error: %s",
					path, strerror(-val));
		}
		return val;
	}
	else if(errno == 0){
		syslog(LOG_ERR, "'%s' does not contain a valid int", path);
	}
	else {
		syslog(LOG_ERR, "Failed to read from '%s': %s",
				path, strerror(errno));
	}
	fclose(f);
	return -EINVAL;
}

static void set_sys(const char *path, int val)
{
	FILE *f = fopen(path, "w");
	if(!f){
		syslog(LOG_ERR, "Failed to open '%s': %s",
				path, strerror(errno));
		return;
	}
	if(fprintf(f, "%d\n", val) < 0){
		syslog(LOG_ERR, "Failed to write %d to '%s': %s",
				val, path, strerror(errno));
	}
	else {
		syslog(LOG_DEBUG, "Wrote %d to '%s'", val, path);
	}
	fclose(f);
}

static void set_sys_str(const char *path, const char *val)
{
	FILE *f = fopen(path, "w");
	if(!f){
		syslog(LOG_ERR, "Failed to open '%s': %s",
				path, strerror(errno));
		return;
	}
	if(fprintf(f, "%s\n", val) < 0){
		syslog(LOG_ERR, "Failed to write %s to '%s': %s",
				val, path, strerror(errno));
	}
	else {
		syslog(LOG_DEBUG, "Wrote %s to '%s'", val, path);
	}
	fclose(f);
}


/*
 * On systems with a VM-BMS, the state of charge is calculated in
 * software based on the battery voltage. The algorithm combines
 * temperature data with the open-circuit voltage of the battery and
 * returns values from a battery-specific table. When the phone is
 * turned on, we can't just measure the open-circuit voltage of the
 * battery, so we have to estimate it. This is done by estimating the
 * changes in the OCV based on voltage and temperature fluctuations and the
 * initial OCV measured when the phone was off (?).
 * The kernel has a table with battery-specific data, which we can query using
 * the BPIOC* ioctls on the battery_data device. The driver also knows how to
 * calculate the SoC from the estimated OCV and current, but the estimations
 * must be done in userspace. On Android, this is done by the proprietary
 * /system/bin/vm_bms daemon. On Bananian, the same functionality is
 * implemented in bpowerd.
 */

static int xchg(struct battery_params *bp)
{
	int ret;
	ret = ioctl(bdata, BPIOCXFCC, bp);
	if(ret < 0){
		syslog(LOG_ERR, "Calculate fcc failed: %s",
				strerror(errno));
		return 0;
	}
	syslog(LOG_DEBUG, "batt_temp %d -> fcc %d mAh",
			bp->batt_temp, bp->fcc_mah);
	ret = ioctl(bdata, BPIOCXSOC, bp);
	if(ret < 0){
		syslog(LOG_ERR, "Calculate soc failed: %s",
				strerror(errno));
		return 0;
	}
	syslog(LOG_DEBUG, "batt_temp %d; ocv %d uV -> soc %d%%",
			bp->batt_temp, bp->ocv_uv, bp->soc);
	ret = ioctl(bdata, BPIOCXSLOPE, bp);
	if(ret < 0){
		syslog(LOG_ERR, "Calculate slope failed: %s",
				strerror(errno));
		return 0;
	}
	syslog(LOG_DEBUG, "batt_temp %d; soc %d%% -> slope %d",
			bp->batt_temp, bp->soc, bp->slope);
	ret = ioctl(bdata, BPIOCXRBATT, bp);
	if(ret < 0){
		syslog(LOG_ERR, "Calculate rbatt_sf failed: %s",
				strerror(errno));
		return 0;
	}
	syslog(LOG_DEBUG, "batt_temp %d; soc %d%% -> rbatt_sf %d%%",
			bp->batt_temp, bp->soc, bp->rbatt_sf);
	return 1;
}

static void next_ocv(int *ocv, int *ibatt, double time, double rbatt,
		double fcc, double slope, int v)
{
	/*
	 * This is a rough approximation (exact copy?) of /system/bin/vm_bms's
	 * algorithm
	 */
	double vdiff = *ocv - v;
	double ocvdiff = vdiff * (5.0 / 18.0)
		/ (rbatt * fcc / time / slope + (5.0 / 36.0));
	double ibatt_d = vdiff * 1000.0
		/ (rbatt + (5.0 / 36.0)) / fcc / time / slope;
	syslog(LOG_DEBUG,
		"t=%fs r=%fmohm fcc=%fAh slope=%f vdiff=%fuV i=%fuA -> %d%+fuV",
		time, rbatt, fcc, slope, vdiff, ibatt_d, *ocv, -ocvdiff);
	*ocv -= ocvdiff;
	*ibatt = ibatt_d;
}

int bms_wait(int fd)
{
	struct battery_params bp;
	struct qpnp_vm_bms_data data;
	int rbatt_unscaled, rbatt_capacitive, rbatt, ocv, ibatt = 0;
	int i;

	if(read(fd, &data, sizeof(data)) != sizeof(data))
		return 0; /* read failed - do not continue */

	set_sys_str("/sys/power/wake_lock", "bananian-bms");

	ocv = bp.ocv_uv = get_sys(SYS_ROOT "voltage_ocv");
	if(bp.ocv_uv < 0) goto err;
	bp.batt_temp = get_sys(SYS_ROOT "temp");
	if(bp.batt_temp < 0) goto err;

	if(!xchg(&bp)) goto err;

	rbatt_unscaled = get_sys(SYS_ROOT "resistance");
	if(rbatt_unscaled < 0) goto err;
	rbatt_capacitive = get_sys(SYS_ROOT "resistance_capacitive");
	if(rbatt_capacitive < 0) goto err;
	rbatt = rbatt_unscaled * bp.rbatt_sf / 100 + rbatt_capacitive;
	syslog(LOG_DEBUG, "rbatt %d + rbatt_cap %d scaled to %d",
			rbatt_unscaled, rbatt_capacitive, rbatt);

	for(i = 0; i < data.num_fifo; i++){
		next_ocv(&ocv, &ibatt,
			0.001 * data.sample_interval_ms * data.sample_count,
			rbatt, 0.001 * bp.fcc_mah, 0.0001 * bp.slope,
			data.fifo_uv[i]);
	}

	set_sys(SYS_ROOT "current_now", ibatt);
	set_sys(SYS_ROOT "voltage_ocv", ocv);

	if(ibatt > 0 && bp.soc < 2){
		int status;
		pid_t child = fork();
		if(child == 0){
			execl("/sbin/poweroff", "/sbin/poweroff",
					(const char*) NULL);
			syslog(LOG_ERR, "Failed to exec poweroff: %s",
					strerror(errno));
			exit(1);
		}
		if(waitpid(child, &status, 0) == child){
			if(WIFEXITED(status) && WEXITSTATUS(status) == 0){
				/*
				 * System should stop now.
				 * Do not wake_unlock, do not continue
				 */
				return 0;
			}
		}
		else {
			syslog(LOG_ERR, "Waitpid for poweroff failed: %s",
					strerror(errno));
		}
	}

err:
	set_sys_str("/sys/power/wake_unlock", "bananian-bms");
	return 1; /* always continue */
}

int get_charge(void)
{
	return get_sys(SYS_ROOT "capacity");
}
