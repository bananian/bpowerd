#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <syslog.h>
#include "daemon.h"

int main(int argc, char * const *argv)
{
	if(argc < 2 || 0 != strcmp(argv[1], "-n")){
		int nullfd;
		nullfd = open("/dev/null", O_RDWR);
		if(nullfd < 0){
			perror("Failed to open /dev/null");
			return 1;
		}
		/* Daemonize */
		switch(fork()){
			case -1:
				perror("fork");
				return 1;
			case 0:
				break;
			default:
				_exit(0);
		}
		setsid();
		if(fork() != 0) _exit(0);
		/* Close standard I/O */
		if(nullfd != 0){
			close(0);
			dup2(nullfd, 0);
		}
		if(nullfd != 1){
			close(1);
			dup2(nullfd, 1);
		}
		if(nullfd != 2){
			close(2);
			dup2(nullfd, 2);
		}
		if(nullfd > 2) close(nullfd);
	}
	/* Start */
	openlog("bpowerd", 0, LOG_DAEMON);
	syslog(LOG_INFO, "Power manager started.");
	mainloop();
	syslog(LOG_INFO, "Power manager terminated.");
	return 0;
}
