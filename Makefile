PROG = bpowerd

PKG_CONFIG = pkg-config
DEPS = dbus-1
CFLAGS = -g -Wall -Wmissing-prototypes -Wmissing-declarations \
	-Wstrict-prototypes -Wformat-nonliteral -Wmissing-format-attribute \
	-O2 -Werror -Wno-error=unused-variable -Wno-error=unused-function \
	-pedantic $(shell $(PKG_CONFIG) --cflags $(DEPS))
LDFLAGS = $(shell $(PKG_CONFIG) --libs $(DEPS))
PREFIX =

OBJECTS = main.o daemon.o bms.o

all: $(PROG)

$(PROG): $(OBJECTS)
	$(CC) -o $@ $^ $(LDFLAGS)

%.service: %.service.in
	@sed 's+@bindir@+$(PREFIX)/sbin+g' $< > $@

install: $(PROG) $(PROG).service
	@install -D -m 755 $(PROG) $(DESTDIR)$(PREFIX)/sbin/$(PROG)
	@install -D -m 644 $(PROG).service $(DESTDIR)$(PREFIX)/lib/systemd/system/$(PROG).service
	@install -D -m 644 $(PROG).conf $(DESTDIR)/etc/dbus-1/system.d/$(PROG).conf

clean:
	rm -f $(PROG) $(OBJECTS) $(PROG).service

