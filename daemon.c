#include <dbus/dbus.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include "daemon.h"
#include "bms.h"

static pid_t child_pid;

struct bpowerd {
	DBusConnection *conn;
};

static int handle_dbus_error(DBusError *err, const char *description, int crit)
{
	if(dbus_error_is_set(err)){
		syslog(crit ? LOG_CRIT : LOG_ERR, "%s: %s",
			description, err->message);
		dbus_error_free(err);
		return 1;
	}
	return 0;
}

static void method_reply_int(DBusConnection *conn, DBusMessage *call, int ret)
{
	DBusMessage *reply;

	reply = dbus_message_new_method_return(call);
	if(!reply){
		syslog(LOG_ERR, "Failed to create integer reply");
		return;
	}

	if(!dbus_message_append_args(reply,
			DBUS_TYPE_INT32, &ret,
			DBUS_TYPE_INVALID))
	{
		dbus_message_unref(reply);
		syslog(LOG_ERR, "Failed to add integer to reply");
		return;
	}

	if(!dbus_connection_send(conn, reply, NULL)){
		syslog(LOG_CRIT, "Could not send reply, out of memory");
	}
	
	dbus_message_unref(reply);
}

static int dbus_init(struct bpowerd *bpowerd)
{
	DBusError err;
	int ret;
	dbus_error_init(&err);

	bpowerd->conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
	handle_dbus_error(&err, "Connection Error", 1);
	if(!bpowerd->conn) return 1;
	syslog(LOG_DEBUG, "Got system bus");

	ret = dbus_bus_request_name(bpowerd->conn, "bananian.PowerManager",
			DBUS_NAME_FLAG_REPLACE_EXISTING |
			DBUS_NAME_FLAG_DO_NOT_QUEUE, &err);
	if(handle_dbus_error(&err, "Could not assign name", 1)){
		return 1;
	}
	if(ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER){
		syslog(LOG_ERR, "Could not assign name, already owned");
		return 1;
	}

	return 0;
}

static void bms_work(struct bpowerd *bpowerd)
{
	int fd = bms_open();
	if(fd < 0){
		syslog(LOG_ERR, "Failed to open BMS: %s\n",
				strerror(errno));
		_exit(0);
	}
	while(bms_wait(fd)){
		DBusMessage *msg = dbus_message_new_signal(
				"/", "bananian.PowerManager", "UpdateCharge");
		int chg = get_charge();
		if(!msg){
			syslog(LOG_ERR, "Failed to create UpdateCharge signal");
			continue;
		}
		if(!dbus_message_append_args(msg,
				DBUS_TYPE_INT32, &chg,
				DBUS_TYPE_INVALID))
		{
			dbus_message_unref(msg);
			syslog(LOG_ERR, "Failed to add argument to signal");
			continue;
		}
		if(!dbus_connection_send(bpowerd->conn, msg, NULL)){
			syslog(LOG_CRIT, "Error sending signal, out of memory");
		}
		dbus_message_unref(msg);
	}
	close(fd);
	_exit(0);
}

void mainloop(void)
{
	struct bpowerd bpowerd;

	if(dbus_init(&bpowerd)) return;

	child_pid = fork();
	if(child_pid == 0) bms_work(&bpowerd);

	while(1){
		DBusMessage *msg;

		dbus_connection_read_write_dispatch(bpowerd.conn, -1);
		msg = dbus_connection_pop_message(bpowerd.conn);
		if(!msg) continue;

		if(dbus_message_is_method_call(msg,
				"bananian.PowerManager",
				"GetCharge"))
		{
			method_reply_int(bpowerd.conn, msg, get_charge());
		}

		dbus_message_unref(msg);
	}
}
